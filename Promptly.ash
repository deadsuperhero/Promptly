// Promptly - Header Declarations
enum Orientation {
  Left,
  Right
};

import int version;
import bool make_choice;
import bool stored_list;
import function the_end();
import function clearScreen();
import function restoreScreen();
import function print(String Output);
import function decisionTime(int dialogID);
import function normalize(String Input);
import function interpret(String TextInput);
import function inputText(String TextInput);
import function show(int spriteNum, Orientation param);
import function hide();