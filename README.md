                                        ______                            _       
                                       (_____ \                      _   | |      
                                        _____) )___ ___  ____  ____ | |_ | |_   _
                                       |  ____/ ___) _ \|    \|  _ \|  _)| | | | |
                                       | |   | |  | |_| | | | | | | | |__| | |_| |
                                       |_|   |_|   \___/|_|_|_| ||_/ \___)_|\__  |
                                                              |_|          (____/  

**Promptly** is a framework designed for old-school Interactive Fiction, running on top of Adventure Game Studio. There are a few central conceits to its designs:

![example](https://codeberg.org/deadsuperhero/Promptly/raw/branch/main/example.gif)

* **Scrolling text**: Promptly uses an alternate `Display()` function, that instead prints text into a scrolling list. With the default styling, it distinctively looks like the output of a DOS prompt or Unix terminal.
* **Parser-based**: Like any old text adventure, the game is entirely driven by text input. This requires a different way of thinking about rooms, items, interactions, and dialog. Promptly does the heavy lifting to make it easy for you!
* **Dialog Navigation**: The Promptly shell is text-driven. We wanted to offer a way to design "Choose Your Own Adventure" type games using the native dialog system, without having the player ever needing to click anything.
* **Graphical Context**: A fun element of many pieces of Interactive Fiction is the ability to render small, simple images inside of the shell to give scenes and conversations variety. The goal is to make this easy for you, without messing up the shell's layout.

## Documentation
You can find a small bit of documentation on how the module works [here](https://codeberg.org/deadsuperhero/Promptly/wiki/Special-Functions).
